# Swipeable

... is a programming lanuage that is easy to swipe on an English swipey smartphone.  I imagine it could be easily ported to other languages.

I should write the language spec here.

But note that there's already a working interpreter (written in perl)

# Syntax

    Command args, args. Command args, args.

The whole language is expressed using keywords, full-stops (periods) and commas.  I chose these because they are on the default keyboard on my phone.  The full-stop (period) acts like the semicolon in many other languages (like C, Java, PHP, Perl) and the comma acts like a comma.  The full-stop (period) is also used for namespacing and the comma is also used for quoting, depending on the context.  The language is largely ignorant of both whitespace and case.  This decision was taken because often swiping will insert spaces and change cases.  A statement-terminating full-stop (period) must be followed by a space.

The command comes first.  Some commands have a special syntax, but most are followed by a space, and then a comma-separated list of arguments.  

New functions can be defined using the Fun command, with the syntax `Fun functionname list,of,argnames.`  Any following lines are interpreted as the body of the function until `End functionname.`  

Strings are defined in a similar way using `Quote stringname.` followed by the string, followed by `End stringname`.

Flow control if done using the If command, which is like the IF in a spreadsheet in that it takes 3 arguments, and if the first evaluates as true it evaluates and returns the value of the second, otherwise it evaluates and returns the value of the third.

You can set a named variable using `Set`.  E.g. `Set myname 20.`

Because there are no parenthses, of you want to call a function/command as an argument to another function command, the arguments of the nested command are separated by pairs of commas.  E.g.  `Say If 2,, message,, 20.` here the pairs of commas indicate that `2`, `message` and `20` are arguments to `If` rather than `Say`.  This system is recursive, so you can have a big complicated command with loads of calls to other commands and loads if commas, if you really want.

Note that Set is a special command that doesn't use commas and therefore pairing commas for a command given as the value for Set is not necessary.  

Commands that don't take arguments need to be distiguished from variable names somehow, and currently this is done using a comma with no args.  If nested, then the right number of commas needs to be used!  e.g. `Say datetime time ,,,.` indicates that time is to be called as a function, not given as a variable.


    Fun say text.     Comment defined a function, name: say, args: text.
        Print text.   Comment print whatever was passed in as text.
        Print chr 10. Comment print a linefeed.
    End say.          Comment end of the function.

    Comment define a quote (string):
    Quote message.    
        Hello, world. Comment Don't put comments here!.
    End message.      Comment end of quote.

    Say message.

    Say datetime time ,,,.

    dump time ,,.
    dump time ,.
    dump time.

    Say q,just a short thing,. Comment but this version does not trim
                                any whitespace.

    Set thing If 0, message, 20.
    Say thing.

    Say If 2,, message,, 20.

    Set mynum 10096.456.
    Say mynum.

# TODO

- functions for adding, subtraction, etc.
- string concatenation
- objects

# Plans

Properties and methods of objects will be written as

    this of that of theother

which would be equivalent to
    
    theother.that.this

in java/javascript/etc.




