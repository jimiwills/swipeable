#!/usr/bin/perl

#use strict;
use warnings;
use Scalar::Util qw(looks_like_number);
use Data::Dumper;
use Date::Format;


## I'm running on a web server, so...
BEGIN {
	print "Content-type: text/plain\n\n";
	*STDERR = *STDOUT;
}

# The environment contains variables used by the code...
my %env = (
	version => 0.01,
	print => sub {my $e = shift; return print @_;},
	'line-feed' => "\n",
	'carriage-return' => "\r",
	'tab' => "\t",
	native => sub {
		my $e = shift; 
		my $c = shift @_; 
		#print "NATIVE: $c ".Dumper(\@_);
		my $r = $c->(@_); 
		#print "NATIVE: $r\n";
		return $r;
	},
	and => sub {
		my $e = shift; 
		foreach(@_){
			return 0 unless $_;
		}
		return 1;
	},
	or => sub {
		my $e = shift; 
		foreach(@_){
			return 1 if $_;
		}
		return 0;
	},
	less => sub {
		my $e = shift; 
		my ($is, $than) = @_;
		if(looks_like_number($is) && looks_like_number($than)){
			return $is < $than;
		}
		else {
			return $is lt $than;
		}
	},
	most => sub {
		my $e = shift; 
		my ($is, $than) = @_;
		if(looks_like_number($is) && looks_like_number($than)){
			return $is <= $than;
		}
		else {
			return $is le $than;
		}
	},
	more => sub {
		my $e = shift; 
		my ($is, $than) = @_;
		if(looks_like_number($is) && looks_like_number($than)){
			return $is > $than;
		}
		else {
			return $is gt $than;
		}
	},
	least => sub {
		my $e = shift; 
		my ($is, $than) = @_;
		if(looks_like_number($is) && looks_like_number($than)){
			return $is >= $than;
		}
		else {
			return $is ge $than;
		}
	},
	time => sub {
		my $e = shift; 
		return time();
	},
	datetime => sub {
		my $e = shift; 
		my $template = '%Y-%m-%d %H:%M:%S %z';
		return time2str($template, $_[0]);
	},
	number => sub {my $e = shift; return $_[0] + 0; },
	dump => sub {my $e = shift; print Dumper @_;},
	dumpenv => sub {
		my $e = shift; 
		print  Dumper $e;
	},
	chr => sub {return chr($_[1])},
	ord => sub {return ord($_[1])},
	'_O' => 1,
	'undef' => undef,
	'' => undef,
	'if' =>'dummy'
);

### some code
my $code = q{


    Fun say text.     Comment defined a function, name: say, args: text.
        Print text.   Comment print whatever was passed in as text.
        Print chr 10. Comment print a linefeed.
    End say.          Comment end of the function.

    Comment define a quote (string):
    Quote message.    
        Hello, world. Comment Don't put comments here!.
    End message.      Comment end of quote.

    Say message.

    Say datetime time ,,,.

    dump time ,,.
    dump time ,.
    dump time.

    Say q,just a short thing,. Comment but this version does not trim
    							any whitespace.

    Set thing If 0, message, 20.
    Say thing.

    Say If 2,, message,, 20.

    Set mynum 10096.456.
    Say mynum.


};


# call interpret with some code and an environment (which is copied!)
interpret ($code, %env);


sub interpret {
	my ($code,%env) = @_; # env copied in so we can mod it without affecting code outside...
	# trim whitespace
	$code =~ s/(?:^\s+|\s+$)//g;
	# inline, anonymous literals
	$code =~ s/\b(q|m|quote|macro)(,+)(.*?[^,])\2(?!,)/
		my $name = lc($1).($env{'_O'}++);
		$env{$name} = $3;
		$name
	/gsie;
	# named literals
	$code =~ s/(^|\.)\s*(macro|quote)\s(\w+)\.\s*(.*?)\n\s*end\s+\3\s*\./
		my $name = lc($3);
		$env{$name} = $4;
		"$1"
	/gsie;
	# inline named literals
	$code =~ s/\b(macro|quote)\s(\w+)\.\s*(.*?)\n\s*end\s+\2/
		my $name = lc($2);
		$env{$name} = $3;
		$name
	/gsie;
	# functions
	$code =~ s/(\.|^)\s*(fun)\s(\w+)\s([\w\s,]+|)\.\s*(.*?)\n\s*end\s+\3\s*\./
		my $name = lc($3);
		my $code = $5;
		my $dot = $1;
		my @args = split m{,\s*}, $4; 
		$env{$name} = sub {
			my $env = shift;
			my %env = %$env;
			@env{@args} = @_;
			interpret($code, %env);
		};
		"$dot"
	/gsie;

	# remove comments...
	$code =~ s/(^|\G|\.)\s*comment[^.]+\./$1/gsi;


	# change things like CORE..gmtime to CORE::gmtime
	$code =~ s/\b\.\.\b/::/g;
	# enable native calls (without quoting the name)
	$code =~ s/\b(native)\s+([\w\:]+)/
		my $name = lc($1).($env{'_O'}++);
		$env{$name} = $2;
		"native $name"
	/gie;

	#print $code;
	# split into sentences (commands)
	my @code = split /\s*\.\s*(?:\s|$)/, $code;

	#print Dumper \@code;
	# run through the commands...
	foreach (@code){
		my $set = 'last';
		$set = lc($1) if s/set\s(\w+)\s//i;
		# save last and any named using set command
		$env{$set} = $env{'last'} =  /\s/ ? runcmd($_,%env) : envval($_,\%env,$_);
	}
}

sub runcmd {
	my ($cmd, %env) = @_;
	#print "$cmd\n";
	# set reference to current env...
	$env{'parentenv'} = $env{'environment'};
	$env{'environment'} = \%env;
	# trim
	$cmd =~ s/(?:^\s+|\s+$)//g;
	# function name
	my ($fun, $args) = split /\s+/, $cmd, 2;
	$fun = lc $fun;
	if(! exists $env{$fun}){
		die "$fun does not exist ($cmd) " . Dumper \%env;
	}
	elsif(! ref($env{$fun}) eq 'CODE'){
		die "$fun is not a subroutine ($cmd) " . Dumper \%env;
	}
	# arguments
	my @args = ();
	if($args){
		## split on a single comma only!
		@args = split /(?<!,),(?!,)/, $args;
		# remove a comma from each comma group
		@args = map {s/(,+),/$1/g; $_} @args;
		@args = map {s/(?:^\s+|\s+$)//g; $_} @args;
		if($fun eq 'if'){
			my ($if, $t, $f) = @args;
			my $tf = $f;
			if($if =~ /\s/ ? runcmd($if,%env) : envval(lc($if),\%env,$cmd)){
				$tf = $t;
			}
			return $tf =~ /\s/ ? runcmd($tf,%env) : envval(lc($tf),\%env,$cmd);
		}
		# run command, if it looks like one, or grab value
		@args = map {/\s/ ? runcmd($_,%env) : envval(lc($_),\%env,$cmd) } @args;
	}
	#print "\nRUNNING $fun @args\n".Dumper (\%env)."\n";
	# call the sub
	return $env{$fun}->(\%env,@args);
}

sub envval {
	# grab a value
	my ($k,$e,$cmd) = @_;
	# numbers are literal
	if(looks_like_number($k)){
		return $k;
	}
	# look up value on key
	elsif(exists $e->{$k}){
		return $e->{$k};
	}
	# or fail
	else {
		#return $k;
		die "$k does not exist ($cmd) ".Dumper $e;
	}
}

